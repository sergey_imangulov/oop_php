<?php

// Класс для хранения номеров моих животных
class Ambar
{
    public $cawCount;
    public $henCount;
    public function __construct($cawCount, $henCount)
    {
        $this->cawCount = $cawCount;
        $this->henCount = $henCount;
    }
}
// Это класс коров, который возвращает случайное количество литров молока
class Caw
{
    public $milk;
    public $id;
    public function __construct()
    {
        // получение случайного идентификатора длиной 3 символа
        $this->id = substr(md5(rand()), 0, 3); 
        $this->milk = rand(8, 12);
    }
}

// курица, которая выдает в виде яиц случайное число от 0 до 1
class Hen
{
    public $eggs;
    public $id; 
    public function __construct()
    {
        $this->id = substr(md5(rand()), 0, 3); // получение случайного идентификатора длиной 3 символа
        $this->eggs = rand(0, 1);
    }
}

//обрабатывать основной класс из всего, что он дает в качестве экземпляра,
// чтобы увидеть, сколько у нас яиц и молока, подсчитав количество животных
class Farm
{
    public $StorageEggs;
    public $StorageMilk;
    public $ambar;
    public function __construct()
    {
        $this->ambar = new Ambar(10, 20);
    }

    public function returnEggs()
    {

        for ($i = 0; $i < $this->ambar->henCount; $i++) {
            $henNew = new Hen;
            $this->StorageEggs += $henNew->eggs;
        }
        echo $this->StorageEggs;
    }
    public function returnMilk()
    {
        for ($i = 0; $i < $this->ambar->cawCount; $i++) {
            $cawNew = new Caw;
            $this->StorageMilk += $cawNew->milk;
        }
        echo $this->StorageMilk;
    }
}

$myFarm = new farm;
echo 'Молока надоено ' . $myFarm->returnMilk() . '<br>';
echo 'Яиц собрано ' . $myFarm->returnEggs() . '<br>';
